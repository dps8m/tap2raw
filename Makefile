# SPDX-License-Identifier: MIT-0
# scspell-id: 2de1b7c8-f783-11ee-b763-80ee73e9b8e7
# Copyright (c) 2024 The DPS8M Development Team

.PHONY: all
all: tap2raw

tap2raw: tap2raw.c

.PHONY: clean
clean:
	rm -f tap2raw
