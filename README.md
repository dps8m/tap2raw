<!-- scspell-id: 4b9d9f6c-ff2a-11ee-810f-80ee73e9b8e7                       -->
<!-- SPDX-License-Identifier: MIT-0                                         -->
<!--                                                                        -->
<!-- Copyright (c) 2024 The DPS8M Development Team                          -->
<!--                                                                        -->
<!-- Permission is hereby granted, free of charge, to any person obtaining  -->
<!-- a copy of this software and associated documentation files (the        -->
<!-- "Software"), to deal in the Software without restriction, including    -->
<!-- without limitation the rights to use, copy, modify, merge, publish,    -->
<!-- distribute, sublicense, and/or sell copies of the Software, and to     -->
<!-- permit persons to whom the Software is furnished to do so.             -->
<!--                                                                        -->
<!-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY              -->
<!-- KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE             -->
<!-- WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND    -->
<!-- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE -->
<!-- LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION -->
<!-- OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION  -->
<!-- WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.        -->

# tap2raw

## Overview

* **`tap2raw`** converts *`tap`* files produced by the
  [DPS8M Simulator](https://dps8m.gitlab.io/) to *`raw`* bitstream format.

## Build

* Run **`make`** to build the software.

## Usage

* `tap2raw < input.tap > output.raw`

## License

* **`tap2raw`** is distributed under the terms of the
  [**MIT-0 License**](LICENSE).

## See also

* [dps8m/**mxload**](https://gitlab.com/dps8m/mxload)
* [dps8m/**dps8m_devel_tools**](https://gitlab.com/dps8m/dps8m_devel_tools)
