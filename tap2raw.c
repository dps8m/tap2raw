/*
 * vim: filetype=c:tabstop=2:softtabstop=2:shiftwidth=2:ai:expandtab
 * scspell-id: 733d6ce2-f781-11ee-8d5c-80ee73e9b8e7
 *
 * SPDX-License-Identifier: MIT-0
 *
 * Copyright (c) 2024 The DPS8M Development Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <errno.h>
#include <inttypes.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#if defined(_WIN32)
# include <fcntl.h>
# include <io.h>
#endif /* if defined( _WIN32 ) */

#define MBUFSIZ (1024 * 64)

static uint64_t wcnt, block, reccnt, mark;
static bool isbe;

static void
status (void)
{
  int64_t ft = ftell (stdin);

  if (ft == -1)
    {
      (void)fprintf (stderr, "Error reading input: %s\r\n", strerror (errno));
      exit (EXIT_FAILURE);
    }

  (void)fprintf (stderr,
                 "Position %-11" PRId64 " Mark %-8" PRIu64 " Block %-6" PRIu64
                 " %5" PRIu64 " record%s\r\n",
                 ft, mark, block, reccnt, reccnt > 1 ? "s" : "");
}

static void
checkbe (void)
{
  int tmp = 1;
  isbe = (*((char *)&tmp) ? false : true);
}

static uint16_t
xhtons (uint16_t x)
{
  if (isbe)
    {
      unsigned char *s = (unsigned char *)&x;
      return (uint16_t)(s[0] << 8 | s[1]);
    }
  else
    {
      return x;
    }
}

int
main (void)
{
  uint64_t bread;
  uint8_t bc[4] = { 0 };
  uint8_t buf[MBUFSIZ];

  checkbe ();

#if defined(_WIN32)
  _setmode (_fileno (stdin), O_BINARY);
  _setmode (_fileno (stdout), O_BINARY);
#endif /* if defined( _WIN32 ) */

  block = 1;
  reccnt = mark = 0;
  do
    {
      (void)memset (buf, 0, sizeof (char) * MBUFSIZ);
      bread = fread (bc, sizeof (char), 4, stdin);
      if (bread == 0)
        {
          break;
        }

      wcnt = ((xhtons ((uint32_t)bc[0] | ((uint32_t)bc[1] << 8)) + 1) & ~1UL);
      if (wcnt)
        {
          (void)fread (buf, sizeof (char), wcnt, stdin);
          (void)fwrite (buf, sizeof (char), wcnt, stdout);
          (void)fread (bc, sizeof (char), 4, stdin);
          reccnt++;
        }
      else
        {
          if (reccnt)
            {
              mark++;
              status ();
            }

          block++;
          reccnt = 0;
        }
    }
  while (1);

  mark++;
  status ();

  return EXIT_SUCCESS;
}
